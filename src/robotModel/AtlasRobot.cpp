#include "../include/robotModel/AtlasRobot.h"
#include <stdexcept>
#include <chrono>
#include <thread>

using namespace planner;
using namespace std::chrono;

/// A single instance of Atlas robot
AtlasRobot::Ptr robotAtlas;

AtlasRobot::AtlasRobot() : RobotModel("Atlas robot", TYPE_BIPED) {
    Foot leftFoot(config.legsNeutral[0]*Quaternion(1,0,0,0).matrix(), true);
    feetNeutral.push_back(leftFoot);
    Foot rightFoot(config.legsNeutral[1]*Quaternion(1,0,0,0).matrix(), true);
    feetNeutral.push_back(rightFoot);
}

/// Construction
AtlasRobot::AtlasRobot(std::string& configFilename) : RobotModel("Atlas robot", TYPE_BIPED), config(configFilename){
    Foot leftFoot(config.legsNeutral[0]*Quaternion(1,0,0,0).matrix(), true);
    feetNeutral.push_back(leftFoot);
    Foot rightFoot(config.legsNeutral[1]*Quaternion(1,0,0,0).matrix(), true);
    feetNeutral.push_back(rightFoot);
}

AtlasRobot::~AtlasRobot(void) {
}

/// select footholds
bool AtlasRobot::selectFootholds(const Robot& initPose, Robot& robot, const ElevationMap& map, bool reverse){
    Foot leftFoot(robot.body * feetNeutral[0].pose,true);
    Foot rightFoot(robot.body * feetNeutral[1].pose, true);
    robot.feet.clear();
    robot.feet.push_back(leftFoot); robot.feet.push_back(rightFoot);

    // determine middle pose
    float_type trans[3] = {(robot.body(0,3)-initPose.body(0,3))/2, (robot.body(1,3)-initPose.body(1,3))/2, (robot.body(2,3)-initPose.body(2,3))/2};
    Mat34 motion = initPose.body.inverse()*robot.body;
    Eigen::Matrix<float_type,3,1> euler = motion.rotation().eulerAngles(0, 1, 2);//x-y-z convention
    euler[0]/=2; euler[1]/=2; euler[2]/=2;
    Robot middlePose(robot);
    Eigen::AngleAxis<double> aX(euler[0], Eigen::Vector3d::UnitX());
    Eigen::AngleAxis<double> aY(euler[1], Eigen::Vector3d::UnitY());
    Eigen::AngleAxis<double> aZ(euler[2], Eigen::Vector3d::UnitZ());
    Eigen::Quaternion<double> q = aX * aY * aZ;
    middlePose.body = initPose.body*( Vec3(trans[0], trans[1], trans[2])*q);
    if (reverse){
        if (!selectFoothold(robot.body, robot.feet[0], 0, config.searchRange, map))
            return false;
        if (!selectFoothold(middlePose.body, robot.feet[1], 1, config.searchRange, map))
            return false;
    }
    else{
        if (!selectFoothold(middlePose.body, robot.feet[0], 0, config.searchRange, map))
            return false;
        if (!selectFoothold(robot.body, robot.feet[1], 1, config.searchRange, map))
            return false;
    }
    return true;
}

/// select foothold
bool AtlasRobot::selectFoothold(const Mat34& robotPose, Foot& foothold, int footNo, float_type range, const ElevationMap& map){
    float_type cost = 100;
    foothold.pose = robotPose * feetNeutral[footNo].pose;
    Foot tmpFoot = foothold; Foot selectedFoothold = foothold;
    Vec3 selectedNormal;
    for(int i=int(-range/map.rasterX);i<int(range/map.rasterX);i++){
        for(int j=int(-range/map.rasterY);j<int(range/map.rasterY);j++){
            tmpFoot.pose(0,3) = foothold.pose(0,3)+i*map.rasterX;
            tmpFoot.pose(1,3) = foothold.pose(1,3)+j*map.rasterY;
            tmpFoot.pose(2,3) = map.get(tmpFoot.pose(0,3), tmpFoot.pose(1,3));
            if (isInsideWorkspace(tmpFoot.pose, footNo, robotPose)) {
                float_type distance = sqrt(pow(i*map.rasterX,2.0)+pow(j*map.rasterY,2.0));
                Mat34 footCoM = tmpFoot.pose * config.footOffset;
                float_type variance = map.computeVariance(footCoM, config.footDimensions.x(), config.footDimensions.y());
                Vec3 normal;
                Vec3 flatv(0,0,1);
                map.normal2surface(footCoM, config.footDimensions.x(), config.footDimensions.y(), normal);
                float_type dotprod = normal.vector().adjoint()*flatv.vector();
                //float_type variance = map.computeVariance(tmpFoot.pose(0,3), tmpFoot.pose(1,3), 2*map.rasterX);
                if ((distance+variance+(dotprod-1)<cost)&&(variance<config.varianceThreshold)) {
                    selectedFoothold = tmpFoot;
                    selectedNormal = normal;
                    cost = distance + variance + (dotprod-1);
                }
            }
        }
    }
    if (cost==100){
        return false;
    }
    else {
        //determine proper foothold orientation
        Vec3 xaxis(selectedFoothold.pose(0,0), selectedFoothold.pose(1,0), selectedFoothold.pose(2,0));
        Vec3 yaxis;
        yaxis.vector() = selectedNormal.vector().cross(xaxis.vector());
        yaxis.x() /= yaxis.vector().norm(); yaxis.y() /= yaxis.vector().norm(); yaxis.z() /= yaxis.vector().norm();
        xaxis.vector() = yaxis.vector().cross(selectedNormal.vector());
        selectedFoothold.pose(0,0) = xaxis.x(); selectedFoothold.pose(1,0) = xaxis.y(); selectedFoothold.pose(2,0) = xaxis.z();
        selectedFoothold.pose(0,1) = yaxis.x(); selectedFoothold.pose(1,1) = yaxis.y(); selectedFoothold.pose(2,1) = yaxis.z();
        selectedFoothold.pose(0,2) = selectedNormal.x(); selectedFoothold.pose(1,2) = selectedNormal.y(); selectedFoothold.pose(2,2) = selectedNormal.z();
        //std::cout << "selected foothold: \n" << selectedFoothold.pose.inverse().matrix() * robot.body.matrix() << "\n";
        foothold = selectedFoothold;
        return true;
    }
}

/// optimize posture
bool AtlasRobot::optimizePosture(Robot& pose, const ElevationMap& map){
    pose.body(2,3) = map.getMax(pose.body(0,3), pose.body(1,3),0.2)+0.9;
    if (pose.feet.size()==0){
        Foot leftFoot(config.legsNeutral[0]*Quaternion(1,0,0,0).matrix(), true);
        leftFoot.pose = pose.body * leftFoot.pose;
        Foot rightFoot(config.legsNeutral[1]*Quaternion(1,0,0,0).matrix(), true);
        rightFoot.pose = pose.body * rightFoot.pose;
        pose.feet.push_back(leftFoot);
        pose.feet.push_back(rightFoot);
    }
    pose.body(0,3) = (pose.feet[0].pose(0,3)+pose.feet[1].pose(0,3))/2.0;
    pose.body(1,3) = (pose.feet[0].pose(1,3)+pose.feet[1].pose(1,3))/2.0;
    return true;
}

/// is robot reference legs poses inside robot workspace
bool AtlasRobot::isInsideWorkspace(const std::vector< Mat34 >& feet, const Mat34& body) const {
    int legNo = 0;
    for (auto it = feet.begin(); it!=feet.end(); it++){
        if (!isInsideWorkspace(*it,legNo,body))
            return false;
        legNo++;
    }
    return true;
}

/// is robot reference legs poses inside robot workspace
bool AtlasRobot::isInsideWorkspace(const Mat34& foot, int footNo, const Mat34& body) const {
    Mat34 footRel = body.inverse() * foot;
    if (footNo==0){
        if ((footRel(0, 3)>config.workspace[0].xlimits.first) && (footRel(0, 3)<config.workspace[0].xlimits.second) && (footRel(1, 3)>config.workspace[0].ylimits.first) && (footRel(1, 3)<config.workspace[0].ylimits.second) && (footRel(2, 3)>config.workspace[0].zlimits.first) && (footRel(2, 3)<config.workspace[0].zlimits.second))
            return true;
        else
            return false;
    }
    else{
        if ((footRel(0, 3)>config.workspace[1].xlimits.first) && (footRel(0, 3)<config.workspace[1].xlimits.second) && (footRel(1, 3)>config.workspace[1].ylimits.first) && (footRel(1, 3)<config.workspace[1].ylimits.second) && (footRel(2, 3)>config.workspace[1].zlimits.first) && (footRel(2, 3)<config.workspace[1].zlimits.second))
            return true;
        else
            return false;
    }
}

planner::RobotModel* planner::createAtlasRobot() {
    robotAtlas.reset(new AtlasRobot());
    return robotAtlas.get();
}

planner::RobotModel* planner::createAtlasRobot(std::string& configFilename) {
    robotAtlas.reset(new AtlasRobot(configFilename));
    return robotAtlas.get();
}
