/** @file planner_defs.h
*
* Planner definitions
*
*/

#ifndef PLANNER_DEFS_H_INCLUDED
#define PLANNER_DEFS_H_INCLUDED

#include <cstdint>
#include <vector>
#include <memory>
#include <cmath>
#include "../../3rdParty/Eigen/Geometry"
#include <fstream>
#include <iostream>

/// flytracker name space
namespace planner {

    /// flytracker default floating point
    typedef double float_type;

    /// 3 element vector class
    typedef Eigen::Translation<float_type,3> Vec3;

    /// Matrix representation of SO(3) group of rotations
    typedef Eigen::Matrix<float_type,3,3> Mat33;

    /// Quaternion representation of SO(3) group of rotations
    typedef Eigen::Quaternion<float_type> Quaternion;

	/// Homogeneous representation of SE(3) rigid body transformations
    typedef Eigen::Transform<double, 3, Eigen::Affine> Mat34;

    /// 2.5D elevation map
    class ElevationMap {
        public:
            /// set of maps
            typedef std::vector<ElevationMap> Seq;

            /// map size
            float_type sizeX, sizeY;

            /// raster size
            float_type rasterX, rasterY;

            /// map size
            size_t numRows, numCols;

            /// Default constructor
            ElevationMap(size_t _numRows, size_t _numCols, float_type _sizeX, float_type _sizeY) :
                numRows(_numRows),
                numCols(_numCols),
                sizeX(_sizeX),
                sizeY(_sizeY),
                rasterX(_sizeX/_numRows),
                rasterY(_sizeY/_numCols),
                map(numRows, std::vector<float_type>(numCols, 0.0)){
            }

            inline float_type get(int row, int col) const {
                return ((row<numRows) && (col<numCols) && (row>-1) && (col>-1)) ? map[col][row] : -1;
            }

            inline float_type get(float_type x, float_type y) const{
                int row, col;
                toRaster(x, y, row, col);
                return get(row, col);
            }

            inline void toRaster(float_type x, float_type y, int& row, int& col) const{
                row = (x/(sizeX/numRows)) + (numRows/2)+1;
                col = (y/(sizeY/numRows)) + (numCols/2)+1;
            }

            float_type getMax(float_type x, float_type y, unsigned int area) const{
                int row, col;
                toRaster(x, y, row, col);
                float_type max=-10;
                for (int i=-int(area); i<=int(area); i++){
                    for (int j=-int(area); j<=int(area); j++) {
                        if (get(row+i, col+j)>max)
                            max = get(row+i, col+j);
                    }
                }
                return max;
            }

            inline bool set(int row, int col, float_type height){
                if ((row>numRows) && (col>numCols) && (row<0) && (col<0)) return false;
                else { map[row][col] = height; return true;}
            }

            bool set(float_type x, float_type y, float_type height){
                int row, col;
                toRaster(x, y, row, col);
                if ((row<numRows) && (col<numCols) && (row<0) && (col<0)) return false;
                else { map[row][col] = height; return true;}
            }

            /// compute variance
            float_type computeVariance(float_type x, float_type y, float_type range) const {
                float_type defaultHeight = get(x,y);
                float_type variance = 0;
                int rastersNoX = range/rasterX; int rastersNoY = range/rasterY;
                for(int i=-rastersNoX;i<=rastersNoX;i++){
                    for(int j=-rastersNoY;j<=rastersNoY;j++){
                        variance+= pow(get(x+i*rasterX, y+j*rasterY)-defaultHeight,2.0);
                    }
                }
                return variance/((rastersNoX*2)+(rastersNoY*2)+2);
            }

            /// compute variance
            float_type computeVariance(Mat34 pose, float_type rangeX, float_type rangeY) const {
                float_type defaultHeight = get(pose(0,3),pose(1,3));
                float_type variance = 0;
                int rastersNoX = rangeX/rasterX; int rastersNoY = rangeY/rasterY;
                for(int i=-rastersNoX;i<=rastersNoX;i++){
                    for(int j=-rastersNoY;j<=rastersNoY;j++){
                        //std::cout << i << " " << j << "\n";
                        Mat34 motion(Mat34::Identity());
                        motion(0,3) = i*rasterX; motion(1,3) = j*rasterY;
                        Mat34 currentPose = pose*motion;
                        //std::cout << "get: " << currentPose(0,3) << ", " <<  currentPose(1,3) << " : " << get(currentPose(0,3), currentPose(1,3)) << std::endl;
                        variance+= pow(get(currentPose(0,3), currentPose(1,3))-defaultHeight,2.0);
                    }
                }
                //std::cout << "var: " << variance <<"\n";
                return variance/((rastersNoX*2)+(rastersNoY*2)+2);
            }

            /// compute spherical variance
            virtual void normal2surface(const Mat34& pose, float_type rangeX, float_type rangeY, Vec3& normal) const = 0;

        protected:
            /// 2.5D map
            std::vector< std::vector<float_type> > map;
    };

    /// Foot
    class Foot {
        public:
            /// set of feet
            typedef std::vector<Foot> Seq;

            /// foot pose
            Mat34 pose;

            /// is foothold
            bool isFoothold;

            /// Constructor
            Foot(Mat34 _footPose, bool _isFoothold) : pose(_footPose), isFoothold(_isFoothold){
            }

            /// show foot state
            void show(void) const {
                std::cout << "pose: \n" << pose.matrix() << std::endl;
                (isFoothold) ? std::cout << "is foothold\n" : std::cout << "is not foothold\n";
            }
    };

    /// Robot
    class Robot {
        public:
            /// set of features
            typedef std::vector<Robot> Seq;

            /// feet poses
            Foot::Seq feet;

            /// body pose
            Mat34 body;

            /// show current pose
            void show(void) const {
                std::cout << "body: \n" << body.matrix() << std::endl;
                int footNo = 0;
                for (Foot::Seq::const_iterator it = feet.begin(); it!=feet.end(); it++){
                    std::cout << "foot " << footNo << ": \n"; it->show();
                    footNo++;
                }
            }
    };

    /// RRT Node
    class RRTNode {
        public:
            /// set of nodes
            typedef std::vector<RRTNode> Seq;

            /// trajectory of robot poses
            Robot::Seq path;

            /// int parent node
            int parent;

            /// int parent node
            unsigned int id;

            /// show node properties
            void show(void) const {
                std::cout << "id: " << id << ", parent: " << parent << std::endl;
                int stateNo = 0;
                for (Robot::Seq::const_iterator it = path.begin(); it!=path.end(); it++){
                    std::cout << "State Number " << stateNo << ": \n"; it->show();
                    stateNo++;
                }
            }
    };

}

#endif // PLANNER_DEFS_H_INCLUDED
