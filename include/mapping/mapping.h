/** @file mapping.h
 *
 * Mapping class
 *
 */

#ifndef _MAPPING_H_
#define _MAPPING_H_

#include "../Defs/planner_defs.h"
#include <iostream>

using namespace planner;

/// 2.5D elevation map
class Map : public ElevationMap {
    public:
        /// Constructor
        Map(size_t _numRows, size_t _numColumns, float_type _sizeX, float_type _sizeY) : ElevationMap(_numRows, _numColumns, _sizeX, _sizeY){
        }

        /// Load elevation map from file
        bool load(std::string filename);

        /// Calculate normal vector (input: vertices of the triangle, output: normal vector)
        Vec3 calculateNormal(std::vector<Vec3>& vertices);

        /// export map to file
        void exportMap(const std::string filename) const;

        /// compute spherical variance
        void normal2surface(const Mat34& pose, float_type rangeX, float_type rangeY, Vec3& normal) const;

     private:
        ///calculate normal to a triangle
        void calculateNormal(const Vec3& p1, const Vec3& p2, const Vec3& p3, Vec3& normal) const;
        /// normalize vector
        void normalizeVector(Vec3& normal) const;
};


#endif // _MAPPING_H_
