#include "../include/RRT/rrt.h"
#include <stdexcept>
#include <chrono>
#include <thread>
#include <fstream>
#include <iostream>

using namespace planner;
using namespace std::chrono;

/// A single instance of RRT planner
RRTPlanner::Ptr plannerRRT;

RRTPlanner::RRTPlanner(float_type _distance2ground, uint_fast16_t _maxIter) :
    Planner("RRT-Connect planner", TYPE_RRTCONNECT) {
    generator.seed(std::random_device{}());
    config.distance2ground = _distance2ground,
    config.maxIter = _maxIter;
}

RRTPlanner::RRTPlanner(std::string& configFilename) :
    Planner("RRT-Connect planner", TYPE_RRTCONNECT), config(configFilename) {
    generator.seed(std::random_device{}());
}

RRTPlanner::~RRTPlanner(void) {
}

///compute distance between poses on the XY plane
float_type RRTPlanner::computeXYDistance(const Mat34& pose1, const Mat34& pose2) const{
    return sqrt(pow(pose1(0,3)-pose2(0,3),2.0)+pow(pose1(1,3)-pose2(1,3),2.0));
}

///compute distance between poses on the XY plane
float_type RRTPlanner::computeXYYawDistance(const Mat34& pose1, const Mat34& pose2) const{
    Vec3 rot1 = toEuler(pose1.rotation());
    Vec3 rot2 = toEuler(pose2.rotation());
    return sqrt(pow(pose1(0,3)-pose2(0,3),2.0)+pow(pose1(1,3)-pose2(1,3),2.0)+pow(rot1.z()-rot2.z(),2.0));
}

///compute yaw rotation angle between two poses
float_type RRTPlanner::computeYawDistance(const Mat34& pose1, const Mat34& pose2) const{
    Mat34 trans = pose1.inverse()*pose2;
    Vec3 rot = toEuler(trans.rotation());
    return rot.z();
}

///find and return nearest neigbour (Euclidean distance)
RRTNode& RRTPlanner::nearestNeighbour(RRTNode::Seq& tree, const RRTNode& qRand){
    float_type distMin = std::numeric_limits<float_type>::infinity();
    unsigned int idxMin=0; unsigned int idx=0;
    for (auto it = tree.begin(); it!=tree.end(); it++){
        float_type dist = computeXYYawDistance(qRand.path.back().body, it->path.back().body);
        if (distMin>dist){
            distMin = dist;
            idxMin = idx;
        }
        idx++;
    }
    return tree[idxMin];
}

/// find new node in direction to goal position
void RRTPlanner::findNodeInDirection2goal(const RRTNode& init, const RRTNode& goal, RRTNode& result, bool reverse, RobotModel* robot){
    Mat34 transform = goal.path.back().body.inverse()*init.path.back().body;
    float_type dist = computeXYDistance(goal.path.back().body, init.path.back().body);
    float_type deltaX = (!reverse) ? goal.path.back().body(0,3)-init.path.back().body(0,3) : init.path.back().body(0,3)-goal.path.back().body(0,3);
    float_type deltaY = (!reverse) ? goal.path.back().body(1,3)-init.path.back().body(1,3) : init.path.back().body(1,3)-goal.path.back().body(1,3);
    Eigen::Matrix<float_type,3,1> initEA = init.path.back().body.rotation().eulerAngles(0, 1, 2);
    Eigen::Matrix<float_type,3,1> goalEA = goal.path.back().body.rotation().eulerAngles(0, 1, 2);
    goalEA(2) = atan2(deltaY,deltaX);
    float_type deltaRotZ = (fabs(goalEA(2)-initEA(2))>M_PI) ? fabs(goalEA(2)-initEA(2)) - M_PI : fabs(goalEA(2)-initEA(2));
    result.path.clear();
    Robot resultPose;
    //std::cout << "init rotz: " <<initEA(2) << "\n";
    //std::cout << "goal rotz: " <<goalEA(2) << "\n";
    //std::cout << "delta rotz: " << deltaRotZ << "\n";
    for (int trialIt = 0; trialIt<config.substepsNo; trialIt++){
        float_type maxDistance = config.maxStepDistance*(float_type(config.substepsNo-trialIt)/float_type(config.substepsNo));
        float_type maxRotz = config.maxRotYaw*(float_type(config.substepsNo-trialIt)/float_type(config.substepsNo));
        if (dist<config.maxStepDistance && dist>config.minStepLength){
            if (deltaRotZ>maxRotz){
                float_type rot;
                if (goalEA(2)-initEA(2)>M_PI)
                    rot = initEA(2)-maxRotz;
                else if (goalEA(2)-initEA(2)<-M_PI)
                    rot = initEA(2)+maxRotz;
                else if (goalEA(2)-initEA(2)<0)
                    rot = initEA(2)-maxRotz;
                else if (goalEA(2)-initEA(2)>0)
                    rot = initEA(2)+maxRotz;
                //float_type rot = (goalEA(2)-initEA(2)>0) ? initEA(2)+MAX_ROTZ : initEA(2)-MAX_ROTZ;
                resultPose.body = Vec3(goal.path.back().body(0,3), goal.path.back().body(1,3), goal.path.back().body(2,3)) * (Eigen::AngleAxis<float_type>(goalEA(0), Eigen::Vector3d::UnitX()) * Eigen::AngleAxis<float_type>(goalEA(1), Eigen::Vector3d::UnitY()) * Eigen::AngleAxis<float_type>(rot, Eigen::Vector3d::UnitZ())).matrix();
          //      std::cout << "final1 rotz: " <<rot << "\n";
            }
            else{
                resultPose.body = Vec3(goal.path.back().body(0,3), goal.path.back().body(1,3), goal.path.back().body(2,3)) * (Eigen::AngleAxis<float_type>(goalEA(0), Eigen::Vector3d::UnitX()) * Eigen::AngleAxis<float_type>(goalEA(1), Eigen::Vector3d::UnitY()) * Eigen::AngleAxis<float_type>(goalEA(2), Eigen::Vector3d::UnitZ())).matrix();
            //    std::cout << "final2 rotz: " <<goalEA(2) << "\n";
            }
        }
        else{
            Mat34 trans(transform);
            trans(0,3) = transform(0,3)*(maxDistance/dist);
            trans(1,3) = transform(1,3)*(maxDistance/dist);
            trans(2,3) = 0;
            if (deltaRotZ>maxRotz){
                float_type rot;
                if (goalEA(2)-initEA(2)>M_PI)
                    rot = initEA(2)-maxRotz;
                else if (goalEA(2)-initEA(2)<-M_PI)
                    rot = initEA(2)+maxRotz;
                else if (goalEA(2)-initEA(2)<0)
                    rot = initEA(2)-maxRotz;
                else if (goalEA(2)-initEA(2)>0)
                    rot = initEA(2)+maxRotz;
                //float_type rot = (goalEA(2)-initEA(2)>0) ? initEA(2)+MAX_ROTZ : initEA(2)-MAX_ROTZ;
                resultPose.body = (Eigen::AngleAxis<float_type>(goalEA(0), Eigen::Vector3d::UnitX()) * Eigen::AngleAxis<float_type>(goalEA(1), Eigen::Vector3d::UnitY()) * Eigen::AngleAxis<float_type>(rot, Eigen::Vector3d::UnitZ())).matrix();
                resultPose.body(0,3) = init.path.back().body(0,3)-trans(0,3); resultPose.body(1,3) = init.path.back().body(1,3)-trans(1,3);
              //  std::cout << "final3 rotz: " << rot << "\n";
            }
            else{
                resultPose.body = (Eigen::AngleAxis<float_type>(goalEA(0), Eigen::Vector3d::UnitX()) * Eigen::AngleAxis<float_type>(goalEA(1), Eigen::Vector3d::UnitY()) * Eigen::AngleAxis<float_type>(goalEA(2), Eigen::Vector3d::UnitZ())).matrix();
                resultPose.body(0,3) = init.path.back().body(0,3)-trans(0,3); resultPose.body(1,3) = init.path.back().body(1,3)-trans(1,3);
                //std::cout << "final4 rotz: " <<goalEA(2) << "\n";
            }
            std::vector<Mat34> feet; for(int i=0;i<init.path.back().feet.size();i++) feet.push_back(init.path.back().feet[i].pose);
            resultPose.body(2,3) = init.path.back().body(2,3);
            // check if transition is possible; if not repeat
            if (robot->isInsideWorkspace(feet, resultPose.body)){
              //  std::cout << "max dist " << trialIt << ", " << maxDistance << "\n";
                break;
            }
            /*else{
                std::cout << "feet \n";
                for(int i=0;i<init.path.back().feet.size();i++)
                    std::cout << feet[i].matrix() << "\n";
                std::cout << "body \n" << resultPose.body.matrix();
                std::cout << "nieudane\n";
                getchar();
            }*/
        }
    }
    resultPose.feet = goal.path.back().feet;
    //std::cout << "to node: "; resultPose.show(); std::cout << "\n";
    result.path.push_back(resultPose);
}

/// Plan foot trajectory from start to goal point
void RRTPlanner::planFootTraj(Foot& start, Foot& goal, unsigned int segmentsNo, unsigned int segmentNo, float_type distance2ground, std::vector<Foot>& path, const ElevationMap& map){
    Foot tmpFoot = start;
    tmpFoot.isFoothold = true;
    path.clear();
    path.push_back(tmpFoot);
    float_type motion[2] = {(goal.pose(0,3)-start.pose(0,3))/(config.trajPoints-2), (goal.pose(1,3)-start.pose(1,3))/(config.trajPoints-2)};
    for (int i=0;i<segmentsNo;i++){
        if (i == segmentNo){
            Mat34 mm = start.pose.inverse()*goal.pose;
            Eigen::Matrix<float_type,3,1> euler = mm.rotation().eulerAngles(0, 1, 2);//x-y-z convention
            euler[0]/=float_type(config.trajPoints-2); euler[1]/=float_type(config.trajPoints-2); euler[2]/=float_type(config.trajPoints-2);
            tmpFoot = path.back(); tmpFoot.isFoothold = false;
            tmpFoot.pose(2,3) = map.getMax(tmpFoot.pose(0,3),tmpFoot.pose(1,3),3) + distance2ground;
            path.push_back(tmpFoot);
            for (int itPath=0; itPath<config.trajPoints-2; itPath++){
                Eigen::AngleAxis<double> aX(euler[0]*(itPath+1), Eigen::Vector3d::UnitX());
                Eigen::AngleAxis<double> aY(euler[1]*(itPath+1), Eigen::Vector3d::UnitY());
                Eigen::AngleAxis<double> aZ(euler[2]*(itPath+1), Eigen::Vector3d::UnitZ());
                Eigen::Quaternion<double> q = aX * aY * aZ;
                tmpFoot.pose = start.pose*(Vec3(0,0,0)*q);
                tmpFoot.pose(0,3) = start.pose(0,3) + motion[0]*(itPath+1); tmpFoot.pose(1,3)=start.pose(1,3) + motion[1]*(itPath+1);

                //tmpFoot.pose(0,3) +=motion[0]; tmpFoot.pose(1,3) +=motion[1];
                tmpFoot.pose(2,3) = map.getMax(tmpFoot.pose(0,3),tmpFoot.pose(1,3),3) + distance2ground;
                tmpFoot.isFoothold = false;
                path.push_back(tmpFoot);
            }
            tmpFoot = goal;
            tmpFoot.isFoothold = true;
            path.push_back(tmpFoot);
        }
        else {
            tmpFoot = path.back();
            for (int itPath=0; itPath<config.trajPoints; itPath++){
                path.push_back(tmpFoot);
            }
        }
    }
}

/// plan feet trajectory between 'start' and 'goal' node
bool RRTPlanner::planTraj(RRTNode& start, RRTNode& goal, float_type distance2ground, RRTNode& newNode, unsigned int gaitType, RobotModel* robot, const ElevationMap& map){
    int feetNo = start.path.back().feet.size();
    int pathSize = 2*config.trajPoints + 1;
    float_type trans[3] = {(goal.path.back().body(0,3)-start.path.back().body(0,3))/float_type(pathSize), (goal.path.back().body(1,3)-start.path.back().body(1,3))/float_type(pathSize), (goal.path.back().body(2,3)-start.path.back().body(2,3))/float_type(pathSize)};
    Mat34 mm = start.path.back().body.inverse()*goal.path.back().body;
    Eigen::Matrix<float_type,3,1> euler = mm.rotation().eulerAngles(0, 1, 2);//x-y-z convention
    euler[0]/=float_type(pathSize); euler[1]/=float_type(pathSize); euler[2]/=float_type(pathSize);
    Robot tmpBody = start.path.back();
    newNode.path.clear();
    for (int itPath=0; itPath<pathSize; itPath++){
        Eigen::AngleAxis<double> aX(euler[0]*(itPath+1), Eigen::Vector3d::UnitX());
        Eigen::AngleAxis<double> aY(euler[1]*(itPath+1), Eigen::Vector3d::UnitY());
        Eigen::AngleAxis<double> aZ(euler[2]*(itPath+1), Eigen::Vector3d::UnitZ());
        Eigen::Quaternion<double> q = aX * aY * aZ;
        tmpBody.body = start.path.back().body*( Vec3(0,0,0)*q);
        tmpBody.body(0,3) = start.path.back().body(0,3) + trans[0]*(itPath+1); tmpBody.body(1,3)=start.path.back().body(1,3) + trans[1]*(itPath+1); tmpBody.body(2,3) = start.path.back().body(2,3) + trans[2]*(itPath+1);
        newNode.path.push_back(tmpBody);
    }
    std::vector<std::vector<Foot>> path;
    path.resize(feetNo);
    for (int itLeg=0; itLeg<feetNo; itLeg++){
        planFootTraj(start.path.back().feet[itLeg], goal.path.back().feet[itLeg], gaitType, itLeg, distance2ground, path[itLeg], map);
    }
    for (int itPath=0; itPath<pathSize; itPath++){
        newNode.path[itPath].feet.clear();
        std::vector<Mat34> feet;
        for (int itLeg=0; itLeg<feetNo; itLeg++){
            newNode.path[itPath].feet.push_back(path[itLeg][itPath]);
            feet.push_back(path[itLeg][itPath].pose);
        }
        if (!robot->isInsideWorkspace(feet, newNode.path[itPath].body)){
            /*for (int itLeg=0; itLeg<feetNo; itLeg++){
                std::cout << "path foot " << itLeg << "\n";
                for (int i=0;i<pathSize; i++){
                    path[itLeg][i].show();
                }
            }
            std::cout << "iter:\n" << itPath << "\n";
            for (int itLeg=0; itLeg<feetNo; itLeg++){
                path[itLeg][itPath].show();
            }

            std::cout << "legs:\n";
            for (int itLeg=0; itLeg<feetNo; itLeg++){
                std::cout << feet[itLeg].matrix() << "\n";
            }
            std::cout << "body: \n";
            std::cout << newNode.path[itPath].body.matrix() << "\n";
            //newNode.show();
            std::cout << "is not inside the workspace of the robot\n";
            //std::cout << "start: \n";
            //start.show();
            //std::cout << "goal: \n";
            //goal.show();
            getchar();*/
            return false;
        }
    }
    return true;
}

Vec3 RRTPlanner::toEuler(const Mat33& R) const{
  Eigen::Quaterniond q(R);
  const double& q0 = q.w();
  const double& q1 = q.x();
  const double& q2 = q.y();
  const double& q3 = q.z();
  double roll = atan2(2*(q0*q1+q2*q3), 1-2*(q1*q1+q2*q2));
  double pitch = asin(2*(q0*q2-q3*q1));
  double yaw = atan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3));
  return Vec3(roll, pitch, yaw);
}

Mat33 RRTPlanner::fromEuler(const Vec3& v) const{
  //UNOPTIMIZED
  double roll  = v.x();
  double pitch = v.y();
  double yaw   = v.z();
  double sy = sin(yaw*0.5);
  double cy = cos(yaw*0.5);
  double sp = sin(pitch*0.5);
  double cp = cos(pitch*0.5);
  double sr = sin(roll*0.5);
  double cr = cos(roll*0.5);
  double w = cr*cp*cy + sr*sp*sy;
  double x = sr*cp*cy - cr*sp*sy;
  double y = cr*sp*cy + sr*cp*sy;
  double z = cr*cp*sy - sr*sp*cy;
  return Eigen::Quaterniond(w,x,y,z).toRotationMatrix();
}

/// finds nodes in direction to the goal poses (divides)
void RRTPlanner::findNodesInDirection2goal(const RRTNode& currentPose, const RRTNode& goalNode, std::vector<RRTNode>& nodes, bool reverse) const{
    float_type deltaX = goalNode.path.back().body(0,3)-currentPose.path.back().body(0,3);
    float_type deltaY = goalNode.path.back().body(1,3)-currentPose.path.back().body(1,3);
    Vec3 initEA = toEuler(currentPose.path.back().body.rotation());
    Vec3 goalEA = toEuler(goalNode.path.back().body.rotation());
    nodes.clear();
    float_type deltaRotZ = goalEA.z()-initEA.z();
    for (int i=config.substepsNo;i>0;i--){
        RRTNode nodeTmp(currentPose);
        nodeTmp.path.back().body = Vec3(0,0,0) * fromEuler(Vec3(initEA.x(), initEA.y(), initEA.z()+(deltaRotZ/double(config.substepsNo))*i));
        nodeTmp.path.back().body(0,3) = currentPose.path.back().body(0,3) + (deltaX/double(config.substepsNo))*i;
        nodeTmp.path.back().body(1,3) = currentPose.path.back().body(1,3) + (deltaY/double(config.substepsNo))*i;
        nodeTmp.path.back().body(2,3) = currentPose.path.back().body(2,3);
        //std::cout << "substeps: \n" << nodeTmp.path.back().body.matrix() << "\n";
        if (computeXYDistance(nodeTmp.path.back().body,currentPose.path.back().body)>config.minStepLength||fabs(computeYawDistance(nodeTmp.path.back().body,currentPose.path.back().body)>config.minRotYaw)){
            nodes.push_back(nodeTmp);
            //std::cout << "added node\n";// << nodeTmp.path.back().body.matrix() << "\n";
        }
    }
    //getchar();
}

/// Extend tree
uint_fast16_t RRTPlanner::extend(RRTNode::Seq& tree, RRTNode& qRand, RRTNode& qNew, RobotModel* robot, const ElevationMap& map, bool reverse, float_type distance2ground, bool tryConnect){
    RRTNode qNear = nearestNeighbour(tree, qRand);
    RRTNode node;
    float_type dist = sqrt(pow(qNear.path.back().body(0,3)-qRand.path.back().body(0,3),2.0)+pow(qNear.path.back().body(1,3)-qRand.path.back().body(1,3),2.0));
    Eigen::Matrix<float_type,3,1> initEA = qNear.path.back().body.rotation().eulerAngles(0, 1, 2);//x-y-z convention
    Eigen::Matrix<float_type,3,1> goalEA = qRand.path.back().body.rotation().eulerAngles(0, 1, 2);
    float_type deltaRotZ = (fabs(goalEA(2)-initEA(2))>M_PI) ? fabs(goalEA(2)-initEA(2)) - M_PI : fabs(goalEA(2)-initEA(2));
    // try to connect two trees
    if ((tryConnect) && (deltaRotZ<config.maxRotYaw) && (dist<config.maxStepDistance) ){
        RRTNode newNode;
        bool success = planTraj(qNear, qRand, distance2ground, newNode, 2, robot, map);
        if (success){
            newNode.parent = qNear.id;
            newNode.id = tree.size();
            tree.push_back(newNode);
            qNew = newNode;
            std::cout << "connect trees\n";
            return 1;
        }
        else {
            //std::cout << "could not connect trees\n";
            return 0;
        }
    }
    // find new node in direction to goal position
    findNodeInDirection2goal(qNear, qRand, node, reverse, robot);
    std::vector<RRTNode> nodes;
    findNodesInDirection2goal(qNear,node,nodes, reverse);
    for (auto itNode=nodes.begin();itNode!=nodes.end();itNode++){
        // set initial height above the ground
        node.path.back().body(2,3) = map.getMax(itNode->path.back().body(0,3), itNode->path.back().body(1,3), 2)+0.9;
        if (robot->selectFootholds(qNear.path.back(), itNode->path.back(), map,reverse)){
            if (robot->optimizePosture(itNode->path.back(), map)){
                if (computeXYDistance(qNear.path.back().body,itNode->path.back().body)>config.minStepLength||fabs(computeYawDistance(qNear.path.back().body,itNode->path.back().body))>config.minRotYaw){
                    /*std::cout << "body1 \n" << qNear.path.back().body.matrix() << "\n";
                    std::cout << "body2 \n" << itNode->path.back().body.matrix() << "\n";
                    std::cout << "dist: " << computeXYDistance(qNear.path.back().body,itNode->path.back().body) << "\n";
                    std::cout << "config.minStepLength " << config.minStepLength << "\n";
                    std::cout << "rot " << fabs(computeYawDistance(qNear.path.back().body,itNode->path.back().body)) << "\n";
                    std::cout << "config.minRotYaw " << config.minRotYaw << "\n";
                    getchar();*/
                    //std::cout << "could not optimize posture\n";
                    RRTNode newNode;
                    bool success = planTraj(qNear, *itNode, distance2ground, newNode, 2, robot, map);
                    if (success){
                        newNode.parent = qNear.id;
                        newNode.id = tree.size();
                        tree.push_back(newNode);
                        qNew = newNode;
                        return 2;
                    }
                }
            }
        }
    }
    return 0;
}

/// create final robot's path
void RRTPlanner::createPath(RRTNode& qBegin, RRTNode& qFinish, RRTNode::Seq& rrtBegin, RRTNode::Seq& rrtFinish){
    plannedPath.clear();
    RRTNode q = qBegin;
    while (q.parent!=-1) {
        for (std::vector<Robot>::reverse_iterator rit = q.path.rbegin(); rit!=q.path.rend(); rit++){
            plannedPath.push_back(*rit);
        }
        q = rrtBegin[q.parent];
    }
    std::reverse(plannedPath.begin(),plannedPath.end());
    q = qFinish;
    while (q.parent!=-1) {
        for (std::vector<Robot>::reverse_iterator rit = q.path.rbegin(); rit!=q.path.rend(); rit++){
            plannedPath.push_back(*rit);
        }
        q = rrtFinish[q.parent];
    }
}

/// Simplify path
void RRTPlanner::simplifyPath(RobotModel* robot, const ElevationMap& map, Robot::Seq& path) {
    std::cout << "Init path size: " << plannedPath.size() << "\n";
    std::vector<std::pair<int,Robot>> posesSeq;
    path.clear();
    for (int i=0;i<plannedPath.size();i++){
        if (i%(2*config.trajPoints + 1)==0) {
            posesSeq.push_back(std::make_pair(i,plannedPath[i]));
        }
    }
    posesSeq.push_back(std::make_pair(plannedPath.size()-1,plannedPath.back()));
    for (int i=0;i<posesSeq.size()-2;i++){
        if (computeXYDistance(posesSeq[i].second.body,posesSeq[i+2].second.body)>config.minStepLength||fabs(computeYawDistance(posesSeq[i].second.body,posesSeq[i+2].second.body))>config.minRotYaw){
            RRTNode newNode;
            RRTNode initNode; initNode.path.push_back(posesSeq[i].second);
            RRTNode goalNode; goalNode.path.push_back(posesSeq[i+2].second);
            bool success = planTraj(initNode, goalNode, config.distance2ground, newNode, 2, robot, map);
            if (success){
                i++;
                for (int itPose = 0; itPose<newNode.path.size();itPose++){
                    path.push_back(newNode.path[itPose]);
                }
                if (i==posesSeq.size()-3){
                    for (int itPose = posesSeq[i+1].first; itPose<posesSeq[i+2].first;itPose++){
                        path.push_back(plannedPath[itPose]);
                    }
                }
            }
            else{
                for (int itPose = posesSeq[i].first; itPose<posesSeq[i+1].first;itPose++){
                    path.push_back(plannedPath[itPose]);
                }
            }
        }
        else {
            for (int itPose = posesSeq[i].first; itPose<posesSeq[i+1].first;itPose++){
                path.push_back(plannedPath[itPose]);
            }
        }
    }
    plannedPath = path;
    std::cout << "Simplified path size: " << plannedPath.size() << "\n";
}

/// plan path using elevation map
Robot::Seq& RRTPlanner::planPath(const Robot& initPose, const Robot& destPose, RobotModel* robot, const ElevationMap& map){
    std::cout << "Start RRT planning...\n";
    high_resolution_clock::time_point start = high_resolution_clock::now();
    initRRT(initPose, destPose, robot, map);
    RRTNode qRand, qNew;
    bool success = false;
    for (int k=0;k<config.maxIter;k++){
        if (k%(config.maxIter/10)==0)
            std::cout << "iteration: " << k << ", tree_begin size: " << rrtBegin.size() << ", tree_finish size: " << rrtFinish.size() << "\n";
        randomConfig(map, qRand);
        if (extend(rrtBegin, qRand, qNew, robot, map, false, config.distance2ground, false)!=0){
            if (extend(rrtFinish, qNew, qRand, robot, map, true, config.distance2ground, true)==1){
                createPath(qNew, qRand, rrtBegin, rrtFinish);
            //    pathSmoothing();
                success = true;
                break;
            }
        }
        randomConfig(map, qRand);
        if (extend(rrtFinish, qRand, qNew, robot, map, true, config.distance2ground, false)!=0){
            //getchar();
            if (extend(rrtBegin, qNew, qRand, robot, map, false, config.distance2ground, true)==1){
                createPath(qRand, qNew, rrtBegin, rrtFinish);
                success = true;
                //pathSmoothing();
                break;
            }
        }
        //if (((k>30)&&(rrtFinish.size()<3)))
           //break;
    }
    if (!success) std::cout << "Planning failed\n";
    std::cout << "begin size: " << rrtBegin.size() << std::endl;
    std::cout << "finish size: " << rrtFinish.size() << std::endl;
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
    std::cout << "RRT planning finished (t = " << elapsed.count() << "ms)\n";

    return plannedPath;
}

/// Init planner
void RRTPlanner::initRRT(const Robot& initPose, const Robot& destPose, RobotModel* robot, const ElevationMap& map){
    rrtBegin.clear(); rrtFinish.clear();
    Robot poseStart(initPose);
    if (config.optimizeInitPose){
        if (!robot->optimizePosture(poseStart, map)) {
            std::cout << "Could not optimize init posture\n";
            throw std::exception();
        }
        if (!robot->selectFootholds(poseStart, poseStart, map, false)) {
            std::cout << "Could not find init footholds\n";
            throw std::exception();
        }
    }
    RRTNode start;
    start.path.push_back(poseStart);
    start.parent=-1;    start.id = 0;
    rrtBegin.push_back(start);
    RRTNode finish;
    Robot pose(destPose);
    if (!robot->optimizePosture(pose, map)) {
        std::cout << "Could not optimize dest posture\n";
        throw std::exception();
    }
    if (!robot->selectFootholds(pose, pose, map, false)) {
        std::cout << "Could not find dest footholds\n";
        throw std::exception();
    }
    finish.path.push_back(pose);
    finish.parent = -1;    finish.id = 0;
    rrtFinish.push_back(finish);
    //rrtBegin[0].show();
    //rrtFinish[0].show();
}

/// Select random config
void RRTPlanner::randomConfig(const ElevationMap& map, RRTNode& randNode){
    std::uniform_real_distribution<float_type> distributionX(-map.sizeX/2.0,map.sizeX/2.0);
    std::uniform_real_distribution<float_type> distributionY(-map.sizeY/2.0,map.sizeY/2.0);
    Robot pose;
    Mat34 body(Vec3(distributionX(generator), distributionY(generator), 0) * Quaternion(1,0,0,0).matrix());
    //Mat34 body(Vec3(-2.0, 0.0, 0)*Quaternion(1,0,0,0).matrix());
    pose.body = body;
    randNode.path.push_back(pose);
}

/// export trees to file
void RRTPlanner::exportTrees(const std::string filename) const{
    std::ofstream file(filename);
    file << "close all;\n clear all;\n";
    for (RRTNode::Seq::const_iterator it = rrtBegin.begin(); it!=rrtBegin.end(); it++){
        file << "plot3([" << it->path.begin()->body(0,3) << ", " << it->path.back().body(0,3) << "], [" << it->path.begin()->body(1,3) << ", " << it->path.back().body(1,3) << "], [" << it->path.begin()->body(2,3) << ", " << it->path.back().body(2,3) << "], 'g', 'LineWidth',3); hold on\n";
    }
    for (RRTNode::Seq::const_iterator it = rrtFinish.begin(); it!=rrtFinish.end(); it++){
        file << "plot3([" << it->path.begin()->body(0,3) << ", " << it->path.back().body(0,3) << "], [" << it->path.begin()->body(1,3) << ", " << it->path.back().body(1,3) << "], [" << it->path.begin()->body(2,3) << ", " << it->path.back().body(2,3) << "], 'r', 'LineWidth',3); hold on\n";
    }
    file.close();
}

/// Draw coordinate system
void RRTPlanner::plotCoordinates(std::ofstream& file, Mat34 pose) const{
    float_type axisLength=0.03;
    file << "plot3([" << pose.matrix()(0,3) << ", " << pose.matrix()(0,3)+pose.matrix()(0,0)*axisLength << "], [" << pose.matrix()(1,3) << ", " << pose.matrix()(1,3)+pose.matrix()(1,0)*axisLength << "], [" << pose.matrix()(2,3) << ", " << pose.matrix()(2,3)+pose.matrix()(2,0)*axisLength << "], 'r', 'LineWidth',1); hold on\n";
    file << "plot3([" << pose.matrix()(0,3) << ", " << pose.matrix()(0,3)+pose.matrix()(0,1)*axisLength << "], [" << pose.matrix()(1,3) << ", " << pose.matrix()(1,3)+pose.matrix()(1,1)*axisLength << "], [" << pose.matrix()(2,3) << ", " << pose.matrix()(2,3)+pose.matrix()(2,1)*axisLength << "], 'g', 'LineWidth',1); hold on\n";
    file << "plot3([" << pose.matrix()(0,3) << ", " << pose.matrix()(0,3)+pose.matrix()(0,2)*axisLength << "], [" << pose.matrix()(1,3) << ", " << pose.matrix()(1,3)+pose.matrix()(1,2)*axisLength << "], [" << pose.matrix()(2,3) << ", " << pose.matrix()(2,3)+pose.matrix()(2,2)*axisLength << "], 'b', 'LineWidth',1); hold on\n";
}

/// export path to file
void RRTPlanner::exportPath(const std::string filename, const planner::Robot::Seq& path) const{
    std::ofstream file(filename);
    file << "close all;\n clear all;\n hold on\n";
    file << "body_x=[";
    for (Robot::Seq::const_iterator it = path.begin(); it!=path.end(); it++){
        file << it->body(0,3) << ",";
    }
    file << "];\n";
    file << "body_y=[";
    for (Robot::Seq::const_iterator it = path.begin(); it!=path.end(); it++){
        file << it->body(1,3) << ",";
    }
    file << "];\n";
    file << "body_z=[";
    for (Robot::Seq::const_iterator it = path.begin(); it!=path.end(); it++){
        file << it->body(2,3) << ",";
    }
    file << "];\n";
    file << "plot3(body_x, body_y, body_z, 'g', 'LineWidth',3); hold on\n";
    if (path.size()>0) {
        for (size_t legNo = 0; legNo<path.back().feet.size(); legNo++){
            file << "foot_x=[";
            for (Robot::Seq::const_iterator it = path.begin(); it!=path.end(); it++){
                file << it->feet[legNo].pose(0,3) << ",";
            }
            file << "];\n";
            file << "foot_y=[";
            for (Robot::Seq::const_iterator it = path.begin(); it!=path.end(); it++){
                file << it->feet[legNo].pose(1,3) << ",";
            }
            file << "];\n";
            file << "foot_z=[";
            for (Robot::Seq::const_iterator it = path.begin(); it!=path.end(); it++){
                file << it->feet[legNo].pose(2,3) << ",";
            }
            file << "];\n";
            file << "plot3(foot_x, foot_y, foot_z, 'r', 'LineWidth',1); hold on\n";
        }
    }
    for (size_t i = 0; i<path.size();i++){
        if ((i%(config.trajPoints*2+1)==0)||(i==path.size()-1)) {
            for (size_t legNo = 0; legNo<path.back().feet.size(); legNo++){
                plotCoordinates(file, path[i].feet[legNo].pose);
                file << "plot3([" << path[i].body(0,3)<< "; " << path[i].feet[legNo].pose(0,3) << "], [" << path[i].body(1,3)<< "; " << path[i].feet[legNo].pose(1,3) << "], [" << path[i].body(2,3)<< "; " << path[i].feet[legNo].pose(2,3) << "], 'r', 'LineWidth',1); hold on\n";
                file << "pause()\n";
            }
            plotCoordinates(file, path[i].body);
            file << "axis([" << path[i].body(0,3)-0.55 << ", " << path[i].body(0,3)+0.55 << ", " << path[i].body(1,3)-0.55 << ", " << path[i].body(1,3)+0.55 << ", " << path[i].body(2,3)-2.4 << ", " << path[i].body(2,3)+0.4 << ", " << "]);\n";
            file << "pause()\n";
        }
        /*if ((i%(config.trajPoints*2+1)==0)&&i>0){
            std::cout << "body1 \n" << path[i].body.matrix() << "\n";
            std::cout << "body2 \n" << path[i-(config.trajPoints*2+1)].body.matrix() << "\n";
            std::cout << "dist: " << computeXYDistance(path[i].body,path[i-(config.trajPoints*2+1)].body);
            getchar();
        }*/
    }
    file.close();
    exportSequence(filename + "dat", path);
}

/// export path to file (stable poses only)
void RRTPlanner::exportSequence(const std::string filename, const planner::Robot::Seq& path) const{
    std::ofstream file(filename);
    file << "#x_r y_r z_r qw_r qx_r qy_r qz_r x_lf y_lf z_lf qw_lf qx_lf qy_lf qz_lf x_rf y_rf z_rf qw_rf qx_rf qy_rf qz_rf\n";
    for (size_t i = 0; i<path.size();i++){
        if ((i%(config.trajPoints*2+1) == 0)||(i==path.size()-1)){
            file << path[i].body(0,3) << " " << path[i].body(1,3) << " " << path[i].body(2,3);
            Quaternion rot(path[i].body.rotation());
            file << " " << rot.w() << " " << rot.x() << " " << rot.y() << " " << rot.z();
            for (int j=0;j<2;j++){
                file << " " << path[i].feet[j].pose(0,3) << " " << path[i].feet[j].pose(1,3) << " " << path[i].feet[j].pose(2,3);
                Quaternion rotlf(path[i].feet[j].pose.rotation());
                file << " " << rotlf.w() << " " << rotlf.x() << " " << rotlf.y() << " " << rotlf.z();
            }
            file << "\n";
        }
    }
    file.close();
}

planner::Planner* planner::createPlannerRRT(float_type distance2ground, uint_fast16_t maxIter) {
    plannerRRT.reset(new RRTPlanner(distance2ground, maxIter));
    return plannerRRT.get();
}

planner::Planner* planner::createPlannerRRT(std::string configFilename) {
    plannerRRT.reset(new RRTPlanner(configFilename));
    return plannerRRT.get();
}
