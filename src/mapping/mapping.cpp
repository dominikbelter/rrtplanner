#include "../include/mapping/mapping.h"
#include <iostream>
#include <memory>

using namespace planner;

bool Map::load(std::string filename){
    std::ifstream file(filename);
    if (file.is_open()){ // open file
        std::string line;
        int lineNo=0; int rowNo=0;
        while ( getline (file,line) ) { // load each line
            std::istringstream is(line);
            //std::cout << line << "\n";
            if (line[0]!='#'){
                if (lineNo==1){
                    is >> numCols >> numRows >> rasterX >> rasterY;
                    sizeX = rasterX*numCols; sizeY = rasterY*numRows;
                    std::cout << "map params: " << numCols << " " << numRows << " " << sizeX << " " << sizeY << "\n";
                    map.clear();
                    map.resize(numRows);
                }
                else{
                    map[numRows-rowNo-1].resize(numCols);
                    for (int i=0; i<numCols; i++)
                         is >> map[numRows-rowNo-1][i];
                    rowNo++;
                }
            }
            lineNo++;
        }
        file.close();
        return true;
    }
    else {
        return false;
    }
}

/// Calculate normal vector (input: vertices of the triangle, output: normal vector)
Vec3 Map::calculateNormal(std::vector<Vec3>& vertices) {
    Vec3 v1(vertices[0].x() - vertices[1].x(), vertices[0].y() - vertices[1].y(), vertices[0].z() - vertices[1].z());
    Vec3 v2(vertices[1].x() - vertices[2].x(), vertices[1].y() - vertices[2].y(), vertices[1].z() - vertices[2].z());

    Vec3 out(v1.y()*v2.z() - v1.z()*v2.y(), v1.z()*v2.x() - v1.x()*v2.z(), v1.x()*v2.y() - v1.y()*v2.x());
    float_type module = sqrt(pow(out.x(),2.0) + pow(out.y(),2.0) + pow(out.z(),2.0));
    out.x() /= module; out.y() /= module; out.z() /= module;
}

/// export map to file
void Map::exportMap(const std::string filename) const{
    std::ofstream file(filename);
    float_type div=1.0;
    //file << "[X,Y]=meshgrid([" << -sizeX/2.0 << ":" << sizeX/(numCols) << ":" << sizeX/2.0 - sizeX/(numCols)<< "],[" << -sizeY/2.0 << ":" << sizeY/(numRows) << ":" << sizeY/2.0 - sizeY/(numRows) << "]);\n";
    file << "[X,Y]=meshgrid([" << -sizeX/2.0 << ":" << sizeX/(div*numCols) << ":" << sizeX/2.0 - sizeX/(div*numCols)<< "],[" << -sizeY/2.0 << ":" << sizeY/(div*numRows) << ":" << sizeY/2.0 - sizeY/(div*numRows) << "]);\n";
    file << "Z=[";
    /*for (auto itRow = map.begin(); itRow!=map.end(); itRow++){
        for (auto itCol = itRow->begin(); itCol!=itRow->end(); itCol++){
            file << *itCol << ", ";
        }
        file << ";\n";
    }*/
    for (double x=-sizeX/2.0; x<sizeX/2.0 - sizeX/(div*numCols); x=x+sizeX/(div*numCols)) {
        for (double y=-sizeY/2.0; y<sizeX/2.0 - sizeX/(div*numCols); y=y+sizeY/(div*numRows)) {
            file << get(y,x) << ", ";
        }
        file << ";\n";
    }

    file << "];\n surf(X,Y,Z); colormap('autumn');\n xlabel('x'); ylabel('y'); zlabel('z');\n";
    file.close();
}

/// compute spherical variance
void Map::normal2surface(const Mat34& pose, float_type rangeX, float_type rangeY, Vec3& normal) const{
    int numX = (rangeX/2) / rasterX;
    int numY = (rangeY/2) / rasterY;
    Mat34 trans; trans.setIdentity();
    std::vector< std::vector<Vec3> > pointSet;
    pointSet.resize(numX*2+1);
    for (int i=0;i<numX*2+1;i++)
        pointSet[i].resize(numY*2+1);
    int indexX=0, indexY=0;
    for (int i=-numX;i<=numX;i++){//create mesh
        indexY=0;
        for (int j=-numY;j<=numY;j++){
            trans(0,3) = rasterX*i; trans(1,3) = rasterY*j;
            Mat34 point = pose * trans;
            float_type height = get(point(0,3), point(1,3));
            Vec3 p3D(point(0,3), point(1,3), height);
            pointSet[indexX][indexY] = p3D;
            indexY++;
        }
        indexX++;
    }
    std::vector<Vec3> normals;
    for (int i=0;i<numX*2;i++){// compute normals
        for (int j=0;j<numY*2;j++){
            Vec3 normal;
            calculateNormal(pointSet[i][j], pointSet[i][j+1], pointSet[i+1][j+1], normal);
            normals.push_back(normal);
            calculateNormal(pointSet[i][j], pointSet[i+1][j+1], pointSet[i+1][j], normal);
            normals.push_back(normal);
        }
    }
    // compute average normal
    float_type sumX=0, sumY=0, sumZ=0;
    for (auto it=normals.begin();it!=normals.end();it++){
        sumX+=(*it).x(); sumY+=(*it).y(); sumZ+=(*it).z();
    }
    normal.x()=sumX/normals.size();
    normal.y()=sumY/normals.size();
    normal.z()=sumZ/normals.size();
    normalizeVector(normal);
}

///calculate normal to a triangle
void Map::calculateNormal(const Vec3& p1, const Vec3& p2, const Vec3& p3, Vec3& normal) const{
    Vec3 v1(p2.x()-p1.x(), p2.y()-p1.y(), p2.z()-p1.z());
    Vec3 v2(p3.x()-p1.x(), p3.y()-p1.y(), p3.z()-p1.z());
    normal.vector() = v2.vector().cross(v1.vector());
    normalizeVector(normal);
}

/// normalize vector
void Map::normalizeVector(Vec3& normal) const {
    float_type norm = normal.vector().norm();
    normal.x() /= norm;    normal.y() /= norm;    normal.z() /= norm;
}
