/** @file rrt.h
 *
 * RRT planner interface
 *
 */

#ifndef _RRT_H_
#define _RRT_H_

#include "planner.h"
#include "../include/robotModel/robot.h"
#include "../../3rdParty/tinyXML/tinyxml2.h"
#include <iostream>
#include <limits>



namespace planner {
    /// create a single planner (RRT-connect)
    Planner* createPlannerRRT(float_type _distance2ground, uint_fast16_t maxIter);
    /// create a single planner (RRT-connect)
    Planner* createPlannerRRT(std::string configFilename);
};

using namespace planner;

/// Planner implementation
class RRTPlanner : public Planner{
    public:
        /// Pointer
        typedef std::unique_ptr<RRTPlanner> Ptr;

        /// Construction
        RRTPlanner(float_type _distance2ground, uint_fast16_t _maxIter);

        /// Construction
        RRTPlanner(std::string& configFilename);

        /// Destructor
        ~RRTPlanner(void);

        /// plan path using elevation map
        Robot::Seq& planPath(const Robot& initPose, const Robot& destPose, RobotModel* robot, const ElevationMap& map);

        /// export trees to file
        void exportTrees(const std::string filename) const;

        /// export path to file
        void exportPath(const std::string filename, const planner::Robot::Seq& path) const;

        /// Simplify path
        void simplifyPath(RobotModel* robot, const ElevationMap& map, Robot::Seq& path);

        class Config{
          public:
            Config() {}
            Config(std::string configFilename){
                tinyxml2::XMLDocument config;
                std::string filename = "../../resources/" + configFilename;
                config.LoadFile(filename.c_str());
                if (config.ErrorID())
                    std::cout << "unable to load robot config file.\n";

                tinyxml2::XMLElement* params = config.FirstChildElement( "Planner" )->FirstChildElement( "parameters" );
                params->QueryDoubleAttribute("distance2ground", &distance2ground);
                params->QueryUnsignedAttribute("maxIter", &maxIter);
                params->QueryBoolAttribute("optimizeInitPose", &optimizeInitPose);
                params->QueryDoubleAttribute("maxStepDistance", &maxStepDistance);
                params->QueryDoubleAttribute("maxRotYaw", &maxRotYaw);
                params->QueryUnsignedAttribute("trajPoints", &trajPoints);
                params->QueryUnsignedAttribute("substepsNo", &substepsNo);
                params->QueryDoubleAttribute("minStepLength", &minStepLength);
                params->QueryDoubleAttribute("minRotYaw", &minRotYaw);
            }
            public:
                /// config - distance to ground
                float_type distance2ground;

                /// config - maximum number of iterations
                unsigned int maxIter;

                ///optimize init pose
                bool optimizeInitPose;

                /// max step length
                float_type maxStepDistance;

                /// max yaw rotation in single step
                float_type maxRotYaw;

                /// trajectory point in single step
                unsigned int trajPoints;

                /// divide step into shorter steps
                unsigned int substepsNo;

                /// minimal step length
                float_type minStepLength;

                /// minimal yaw rotation in single step
                float_type minRotYaw;
        };

    private:
        ///Configuration of the module
        Config config;

        /// RRT_begin tree
        RRTNode::Seq rrtBegin;

        /// RRT_finish tree
        RRTNode::Seq rrtFinish;

        /// Random number Generator
        std::default_random_engine generator;

        /// Init planner
        void initRRT(const Robot& initPose, const Robot& destPose, RobotModel* robot, const ElevationMap& map);

        /// Select random config
        void randomConfig(const ElevationMap& map, RRTNode& randNode);

        /// Extend tree
        uint_fast16_t extend(RRTNode::Seq& tree, RRTNode& qRand, RRTNode& qNew, RobotModel* robot, const ElevationMap& map, bool reverse, float_type distance2ground, bool tryConnect);

        ///find and return nearest neigbour (Euclidean distance)
        RRTNode& nearestNeighbour(RRTNode::Seq& tree, const RRTNode& qRand);

        /// find new node in direction to goal position
        void findNodeInDirection2goal(const RRTNode& init, const RRTNode& goal, RRTNode& result, bool reverse, RobotModel* robot);

        /// Plan foot trajectory from start to goal point
        void planFootTraj(Foot& start, Foot& goal, unsigned int segmentsNo, unsigned int segmentNo, float_type distance2ground, std::vector<Foot>& path, const ElevationMap& map);

        /// plan feet trajectory between 'start' and 'goal' node
        bool planTraj(RRTNode& start, RRTNode& goal, float_type distance2ground, RRTNode& newNode, unsigned int gaitType, RobotModel* robot, const ElevationMap& map);

        /// create final robot's path
        void createPath(RRTNode& qBegin, RRTNode& qFinish, RRTNode::Seq& rrtBegin, RRTNode::Seq& rrtFinish);

        /// Draw coordinate system
        void plotCoordinates(std::ofstream& file, Mat34 pose) const;

        /// export path to file (stable poses only)
        void exportSequence(const std::string filename, const planner::Robot::Seq& path) const;

        ///compute distance between poses on the XY plane
        float_type computeXYDistance(const Mat34& pose1, const Mat34& pose2) const;

        ///compute distance between poses on the XY plane
        float_type computeXYYawDistance(const Mat34& pose1, const Mat34& pose2) const;

        ///compute yaw rotation angle between two poses
        float_type computeYawDistance(const Mat34& pose1, const Mat34& pose2) const;

        /// finds nodes in direction to the goal poses (divides)
        void findNodesInDirection2goal(const RRTNode& currentPose, const RRTNode& goalNode, std::vector<RRTNode>& nodes, bool reverse) const;

        Vec3 toEuler(const Mat33& R) const;
        Mat33 fromEuler(const Vec3& v) const;
};

#endif // _RRT_H_
