#include "include/Defs/planner_defs.h"
#include "RRT/rrt.h"
#include "robotModel/AtlasRobot.h"
#include "mapping/mapping.h"
#include "3rdParty/tinyXML/tinyxml2.h"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace planner;

int main( int argc, const char** argv )
{
    try {
        tinyxml2::XMLDocument config;
        config.LoadFile("../../resources/configGlobal.xml");
        if (config.ErrorID())
            std::cout << "unable to load config file.\n";
        std::string plannerName(config.FirstChildElement( "Planner" )->FirstChildElement( "name" )->GetText());

        // elevation map
        Map map(600, 600, 6.0, 6.0);
        map.load("../../resources/maps/trasa4.dat");


        //create planner
        std::cout << "Create planner: " << plannerName << std::endl;
        Planner* planner = createPlannerRRT("configGlobal.xml");

        float_type quat[4];
        float_type pos[3];
        config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("x", &pos[0]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("y", &pos[1]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("z", &pos[2]);
        config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("qw", &quat[0]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("qx", &quat[1]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("qy", &quat[2]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("qz", &quat[3]);

        Robot initPose; initPose.body = Vec3(0,0,0)*Quaternion(quat[0],quat[1],quat[2],quat[3]).matrix();
        initPose.body(0,3) = pos[0]; initPose.body(1,3) = pos[1]; initPose.body(2,3) = pos[2];
        std::cout << "Init pose: " << initPose.body.matrix()(0,3) << ", " << initPose.body.matrix()(1,3) << ", " << initPose.body.matrix()(2,3) << std::endl;

        config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg0" )->QueryDoubleAttribute("x", &pos[0]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg0" )->QueryDoubleAttribute("y", &pos[1]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg0" )->QueryDoubleAttribute("z", &pos[2]);
        config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg0" )->QueryDoubleAttribute("qw", &quat[0]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg0" )->QueryDoubleAttribute("qx", &quat[1]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg0" )->QueryDoubleAttribute("qy", &quat[2]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg0" )->QueryDoubleAttribute("qz", &quat[3]);
        Foot leftFoot(Vec3(0,0,0)*Quaternion(quat[0],quat[1],quat[2],quat[3]).matrix(), true);
        leftFoot.pose(0,3) = pos[0]; leftFoot.pose(1,3) = pos[1]; leftFoot.pose(2,3) = pos[2];
        initPose.feet.push_back(leftFoot);

        config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg1" )->QueryDoubleAttribute("x", &pos[0]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg1" )->QueryDoubleAttribute("y", &pos[1]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg1" )->QueryDoubleAttribute("z", &pos[2]);
        config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg1" )->QueryDoubleAttribute("qw", &quat[0]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg1" )->QueryDoubleAttribute("qx", &quat[1]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg1" )->QueryDoubleAttribute("qy", &quat[2]); config.FirstChildElement( "Robot" )->FirstChildElement( "initPose" )->FirstChildElement( "leg1" )->QueryDoubleAttribute("qz", &quat[3]);
        Foot rightFoot(Vec3(0,0,0)*Quaternion(quat[0],quat[1],quat[2],quat[3]).matrix(), true);
        rightFoot.pose(0,3) = pos[0]; rightFoot.pose(1,3) = pos[1]; rightFoot.pose(2,3) = pos[2];
        initPose.feet.push_back(rightFoot);

        Robot destPose;
        config.FirstChildElement( "Robot" )->FirstChildElement( "destPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("x", &pos[0]); config.FirstChildElement( "Robot" )->FirstChildElement( "destPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("y", &pos[1]); config.FirstChildElement( "Robot" )->FirstChildElement( "destPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("z", &pos[2]);
        config.FirstChildElement( "Robot" )->FirstChildElement( "destPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("qw", &quat[0]); config.FirstChildElement( "Robot" )->FirstChildElement( "destPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("qx", &quat[1]); config.FirstChildElement( "Robot" )->FirstChildElement( "destPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("qy", &quat[2]); config.FirstChildElement( "Robot" )->FirstChildElement( "destPose" )->FirstChildElement( "pose" )->QueryDoubleAttribute("qz", &quat[3]);
        destPose.body = Vec3(0,0,0)*Quaternion(quat[0],quat[1],quat[2],quat[3]).matrix();
        destPose.body(0,3) = pos[0]; destPose.body(1,3) = pos[1]; destPose.body(2,3) = pos[2];
        std::cout << "Goal pose: " << destPose.body.matrix()(0,3) << ", " << destPose.body.matrix()(1,3) << ", " << destPose.body.matrix()(2,3) << std::endl;

        std::string robotConfigFilename(config.FirstChildElement( "Robot" )->FirstChildElement( "configFile" )->GetText());
        RobotModel* robot = createAtlasRobot(robotConfigFilename);

        //plan path
        std::cout << "start planing\n";
        planner::Robot::Seq path = planner->planPath(initPose, destPose, robot, map);
        planner->exportPath("../../resources/initpath.m", path);
        planner->simplifyPath(robot,map, path);
        std::cout << "Export map\n";
        map.exportMap("../../resources/map.m");
        std::cout << "Export trees\n";
        planner->exportTrees("../../resources/trees.m");
        std::cout << "Export path\n";
        planner->exportPath("../../resources/path.m", path);
    }
    catch (const std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        return 1;
    }
    return 0;
}
