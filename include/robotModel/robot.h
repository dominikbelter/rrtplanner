/** @file robot.h
 *
 * Robot model interface
 *
 */

#ifndef _ROBOT_H_
#define _ROBOT_H_

#include "../Defs/planner_defs.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <limits>
#include <iomanip>

namespace planner {
    /// Robot model interface
    class RobotModel {
        public:

            /// Robot type
            enum Type {
                    /// BIPED robot
                    TYPE_BIPED,
                    /// Quadruped robot
                    TYPE_QUADRUPED,
                    /// Hexapod robot
                    TYPE_HEXAPOD,
            };

            /// overloaded constructor
            RobotModel(const std::string _name, Type _type) : name(_name), type(_type) {};

            /// Name of the planner
            virtual const std::string& getName() const { return name; }

            /// select footholds
            virtual bool selectFootholds(const Robot& initPose, Robot& pose, const ElevationMap& map, bool reverse) = 0;

            /// select foothold
            virtual bool selectFoothold(const Mat34& robotPose, Foot& foothold, int footNo, float_type range, const ElevationMap& map) = 0;

            /// optimize posture
            virtual bool optimizePosture(Robot& pose, const ElevationMap& map) = 0;

            /// is robot reference legs poses inside robot workspace
            virtual bool isInsideWorkspace(const std::vector< Mat34 >& feet, const Mat34& body) const = 0;

            /// Virtual descrutor
            virtual ~RobotModel() {}

        protected:
            /// Robot type
            Type type;

            /// Robot name
            const std::string name;

            /// neutral legs positions
            std::vector<Foot> feetNeutral;
    };
};


#endif // _ROBOT_H_
