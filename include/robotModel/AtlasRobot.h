/** @file AtlasRobot.h
 *
 * Atlas robot interface
 *
 */

#ifndef _ATLAS_ROBOT_H_
#define _ATLAS_ROBOT_H_

#include "../Defs/planner_defs.h"
#include "robot.h"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <limits>
#include "../../3rdParty/tinyXML/tinyxml2.h"

namespace planner {
    /// create a single robot model (Atlas)
    RobotModel* createAtlasRobot();
    /// create a single robot model (Atlas)
    RobotModel* createAtlasRobot(std::string& configFilename);
};

using namespace planner;

/// Atlas robot implementation
class AtlasRobot : public RobotModel{
    public:
        /// Pointer
        typedef std::unique_ptr<AtlasRobot> Ptr;

        /// Construction
        AtlasRobot(void);

        /// Construction
        AtlasRobot(std::string& configFilename);

        /// Destructor
        ~AtlasRobot(void);

        /// plan path using elevation map
        Robot::Seq& planPath(const Robot& initPose, const Robot& destPose, const ElevationMap& map);

        /// select footholds
        bool selectFootholds(const Robot& initPose, Robot& pose, const ElevationMap& map, bool reverse);

        /// select foothold
        bool selectFoothold(const Mat34& robotPose, Foot& foothold, int footNo, float_type range, const ElevationMap& map);

        /// optimize posture
        bool optimizePosture(Robot& pose, const ElevationMap& map);

        /// is robot reference legs poses inside robot workspace
        bool isInsideWorkspace(const std::vector< Mat34 >& feet, const Mat34& body) const;

        /// is robot reference legs poses inside robot workspace
        bool isInsideWorkspace(const Mat34& foot, int footNo, const Mat34& body) const;

        class Config{
          public:
            class BoxWorkspace {
            public:
                std::pair<float_type, float_type> xlimits;
                std::pair<float_type, float_type> ylimits;
                std::pair<float_type, float_type> zlimits;
            };

            Config() {}
            Config(std::string& configFilename){
                tinyxml2::XMLDocument config;
                std::string filename = "../../resources/" + configFilename;
                config.LoadFile(filename.c_str());
                if (config.ErrorID())
                    std::cout << "unable to load robot config file.\n";
                tinyxml2::XMLElement * model = config.FirstChildElement( "Kinematic" );
                workspace.resize(2);
                model->FirstChildElement( "workspace" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("xmin", &workspace[0].xlimits.first);
                model->FirstChildElement( "workspace" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("xmax", &workspace[0].xlimits.second);
                model->FirstChildElement( "workspace" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("ymin", &workspace[0].ylimits.first);
                model->FirstChildElement( "workspace" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("ymax", &workspace[0].ylimits.second);
                model->FirstChildElement( "workspace" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("zmin", &workspace[0].zlimits.first);
                model->FirstChildElement( "workspace" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("zmax", &workspace[0].zlimits.second);
                //right leg
                model->FirstChildElement( "workspace" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("xmin", &workspace[1].xlimits.first);
                model->FirstChildElement( "workspace" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("xmax", &workspace[1].xlimits.second);
                model->FirstChildElement( "workspace" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("ymin", &workspace[1].ylimits.first);
                model->FirstChildElement( "workspace" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("ymax", &workspace[1].ylimits.second);
                model->FirstChildElement( "workspace" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("zmin", &workspace[1].zlimits.first);
                model->FirstChildElement( "workspace" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("zmax", &workspace[1].zlimits.second);
                // neutral position
                legsNeutral.resize(2);
                model->FirstChildElement( "neutralPosition" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("x", &legsNeutral[0].x());
                model->FirstChildElement( "neutralPosition" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("y", &legsNeutral[0].y());
                model->FirstChildElement( "neutralPosition" )->FirstChildElement( "LeftLeg" )->QueryDoubleAttribute("z", &legsNeutral[0].z());
                model->FirstChildElement( "neutralPosition" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("x", &legsNeutral[1].x());
                model->FirstChildElement( "neutralPosition" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("y", &legsNeutral[1].y());
                model->FirstChildElement( "neutralPosition" )->FirstChildElement( "RightLeg" )->QueryDoubleAttribute("z", &legsNeutral[1].z());
                // foot
                footOffset.setIdentity();
                model->FirstChildElement( "foot" )->FirstChildElement( "offset" )->QueryDoubleAttribute("x", &footOffset(0,3));
                model->FirstChildElement( "foot" )->FirstChildElement( "offset" )->QueryDoubleAttribute("y", &footOffset(1,3));
                model->FirstChildElement( "foot" )->FirstChildElement( "offset" )->QueryDoubleAttribute("z", &footOffset(2,3));
                model->FirstChildElement( "foot" )->FirstChildElement( "dimensions" )->QueryDoubleAttribute("x", &footDimensions.x());
                model->FirstChildElement( "foot" )->FirstChildElement( "dimensions" )->QueryDoubleAttribute("y", &footDimensions.y());
                model->FirstChildElement( "foot" )->FirstChildElement( "dimensions" )->QueryDoubleAttribute("z", &footDimensions.z());
                // foothold selection
                model->FirstChildElement( "footholdSelection" )->FirstChildElement( "parameters" )->QueryDoubleAttribute("varianceThreshold", &varianceThreshold);
                model->FirstChildElement( "footholdSelection" )->FirstChildElement( "parameters" )->QueryDoubleAttribute("searchRange", &searchRange);
            }
            public:
                /// workspace of the legs
                std::vector<BoxWorkspace> workspace;
                ///legs neutral position
                std::vector<Vec3> legsNeutral;
                ///foot offset
                Mat34 footOffset;
                /// foot dimensions (box)
                Vec3 footDimensions;
                /// foothold selection: variance threshold
                float_type varianceThreshold;
                /// foothold selection: search range
                float_type searchRange;
        };

    private:
        ///Configuration of the module
        Config config;
};

#endif // _ATLAS_ROBOT_H_
