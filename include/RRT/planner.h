#ifndef PLANNER_H
#define PLANNER_H

#include "../Defs/planner_defs.h"
#include "../include/robotModel/robot.h"

namespace planner {
    /// Planner interface
    class Planner {
        public:

            /// Planner type
            enum Type {
                    /// RRT-Connect
                    TYPE_RRTCONNECT,
            };

            /// overloaded constructor
            Planner(const std::string _name, Type _type) : name(_name), type(_type) {};

            /// Name of the planner
            virtual const std::string& getName() const { return name; }

            /// plan path using elevation map
            virtual Robot::Seq& planPath(const Robot& initPose, const Robot& destPose, RobotModel* robot, const ElevationMap& map) = 0;

            /// export trees to file
            virtual void exportTrees(const std::string filename) const = 0;

            /// export path to file
            virtual void exportPath(const std::string filename, const planner::Robot::Seq& path) const = 0;

            /// Simplify path
            virtual void simplifyPath(RobotModel* robot, const ElevationMap& map, Robot::Seq& path) = 0;

            /// Virtual descrutor
            virtual ~Planner() {}

        protected:
            /// Detector type
            Type type;

            /// Detector name
            const std::string name;

            /// Set of objects
            Robot::Seq plannedPath;
    };
};

#endif // PLANNER_H
